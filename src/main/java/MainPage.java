import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Admin on 18.06.2016.
 */
public class MainPage extends Page {
    public MainPage() {
        driver.get("http://xbsoftware.com/");
        PageFactory.initElements(driver, this);
    }

    @FindBy(css = "div.top-header.present-top-header a.contacts-link")
    private WebElement contactUsButton;

    @FindBy(xpath = "//input[@name='user_name']")
    private WebElement contactUsNameInput;

    @FindBy(xpath = "//input[@name='user_company']")
    private WebElement contactUsCompanyInput;

    public MainPage clickOnContactUsButton() {
        contactUsButton.click();
        return this;
    }

    public MainPage inputNameInContactUs(String name) {
        contactUsNameInput.sendKeys(name);
        return this;
    }

    public MainPage inputCompanyInContactUs(String company) {
        contactUsCompanyInput.sendKeys(company);
        return this;
    }
}
