import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

public class BaseTest {

    public static WebDriver driver;

    @BeforeMethod
    public void beforeTestMethod() {

        Page.setDriver(new FirefoxDriver());
        driver = Page.getDriver();
        driver.manage().window().maximize();
    }

    @AfterMethod
    public void afterTestMethod() {
        if (driver != null) {
            driver.close();
        }
    }
}
